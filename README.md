# Temporally improved volume registration by data driven subvolume interpolation of simultaneous multislice EPI time series


## Description
This is a repository providing a MATLAB implementation of the temporal upsampling method described in "Choreography Controlled (ChoCo) brain MRI artifact
generation for labelled motion-corrupted datasets" (https://www.sciencedirect.com/science/article/pii/S1120179722020476). 

Temporal upsampling is performed based on the SMS scheme, which can be obtained in the metadata of DICOM files. The problem of multi-slice-to-volume registration is translated to volume-to-volume registration by spatial interpolation of partial volumes, arranged according to the SMS scheme. A full SMS-EPI acquisition is split into multiple time series and individually interpolated and realigned to produce matrices of motion parameters, which can then be temporally interleaved to produce a final upsampled motion estimation.
Rigid-body volume registration is performed with the SPM toolbox.

## Contents

We provide a working Matlab demo (requirements: Matlab 2018b or higher - the demo was tested and works on Matlab 2018b and 2020a).
The main script to run is `main_create_ts_withLibmri.m` and is located in the `./choco_mosaic folder`. It will save results in the `./data/results/` folder.
Helper functions used by the main script can be found in `./choco_mosaic/libMRI`.

The data for one of our subjects is provided in `./data` and is used to run the demo.


## Our published paper
The final published version of our paper is freely available and can be found here: 

https://www.sciencedirect.com/science/article/pii/S1120179722020476

DOI: https://doi.org/10.1016/j.ejmp.2022.09.005

## Citing
If you find this work useful/relevant for your research, please consider citing it as:

@article{dabrowski2022choreography,
  title={Choreography Controlled (ChoCo) brain MRI artifact generation for labeled motion-corrupted datasets},
  author={Dabrowski, Oscar and Courvoisier, S{\'e}bastien and Falcone, Jean-Luc and Klauser, Antoine and Songeon, Julien and Kocher, Michel and Chopard, Bastien and Lazeyras, Fran{\c{c}}ois},
  journal={Physica Medica},
  volume={102},
  pages={79--87},
  year={2022},
  publisher={Elsevier}
}

## License


## Project status
A working Matlab demo is available to download. (Tested on Matlab 2018b and 2020a).
