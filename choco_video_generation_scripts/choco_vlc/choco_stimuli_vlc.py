import os
import subprocess
import argparse
import platform


### EXAMPLE CALL (powerShell): python .\choco_stimuli_vlc.py C:\Users\odabr\Videos\forFMRI\videos_fMRI

#****************************************************************************
## Global vars (params that should be fixed depending on platform/stimulus PC)

MRI_signal_char = '5'
NEXT_char = 'n'
EXIT_char = 'q'
vlcExecutable = r'C:\Program Files (x86)\VideoLAN\VLC\vlc.exe'

#****************************************************************************


def play_videos(srcDir):
    
    # Check if VLC exists
    if not os.path.isfile(vlcExecutable):
        print(f"\nError: VLC not found at '{vlcExecutable}'. Please ensure VLC is installed.")
        return
    
    # Sorted list of videos based on filename prefix
    video_files = sorted(
        [f for f in os.listdir(srcDir) if f.lower().endswith(('.mp4', '.mkv', '.avi'))],
        key=lambda x: x.split('_', 1)[0]
    )
    
    played_videos = set()
    
    for video in video_files:
        video_path = os.path.join(srcDir, video)
        if video not in played_videos:
            print(f"\nReady to play: {video}. Waiting for '{MRI_signal_char}' to start ...")
            
            # Wait for MRI_signal_char key press
            if wait_for_key(MRI_signal_char) == EXIT_char:
                print("\nQuitting the script...")
                return
            
            # Launch VLC in fullscreen mode
            print(f"\nPlaying: {video}")
            subprocess.run([vlcExecutable, '--fullscreen', video_path], check=True)
            played_videos.add(video)
            
            if len(played_videos) == len(video_files):
                print("All videos have been played.")
                return
        
            print("\nPress 'n' to wait for the next signal ...")
            if wait_for_key(NEXT_char) == EXIT_char:
                print("\nQuitting the script...")
                return
            
            
    


def wait_for_key(key=MRI_signal_char):
    ### Wait for a specific key press without requiring Enter (Windows-only).
    if platform.system() != "Windows":
        print(">>>>> This script's immediate key detection is only supported on Windows.")
        return
    
    import msvcrt
    while True:
        char = msvcrt.getch().decode('utf-8')
        if char == EXIT_char:
            return EXIT_char  # Signal to quit
        if char == key:
            break


#########################################################################


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Play videos in order based on filename prefix.")
    parser.add_argument(
        'srcDir', 
        type=str, 
        nargs='?',  # Make the argument optional
        default=os.getcwd(),  
        help="Source directory containing the video files. Defaults to the current directory."
    )
    
    args = parser.parse_args()
    
    srcDir = args.srcDir

    # Display found video files
    video_files = [f for f in os.listdir(srcDir) if f.lower().endswith(('.mp4', '.mkv', '.avi'))]
    print('\n>>>>> Found the following video files:')
    for video in sorted(video_files):
        print(video)
    
    if not os.path.isdir(srcDir):
        print(f"Error: The directory '{srcDir}' does not exist.")
    elif not video_files:
        print("No video files found in the directory.")
    else:
        play_videos(srcDir)
