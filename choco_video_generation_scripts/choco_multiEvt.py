import cv2
import numpy as np

background_image_path = "regleEcran2axes.png"
background = cv2.imread(background_image_path)

if background is None:
    raise FileNotFoundError(f"Could not load the background image at {background_image_path}. Please check the file path.")

# Video params
fps = 60
video_resolution = (1920, 1080)
dot_radius = 50
pixels_per_degree = 75  # 1 degree = 75 pixels

# Font parameters
font = cv2.FONT_HERSHEY_SIMPLEX
font_scale = 1.2
font_thickness = 3
timer_flash_duration = 2  # Seconds the arrows and cross will appear before motion starts
static_dot_color = (0, 0, 255)  # Red for static dot
moving_dot_color = (0, 255, 0)  # Green for moving dot
font_color = (0, 0, 0)  # Black for timer and arrows
cross_color = (0, 0, 255)  # Red color for the cross (same as static dot)


def draw_centered_text(img, text, center, font, font_scale, color, thickness, offset_y=0):
    text_size, _ = cv2.getTextSize(text, font, font_scale, thickness)
    text_x = int(center[0] - text_size[0] / 2)
    text_y = int(center[1] + text_size[1] / 2 + offset_y)
    cv2.putText(img, text, (text_x, text_y), font, font_scale, color, thickness)


def draw_cross(img, center, size, color, thickness):
    x, y = center
    # Draw horizontal line
    cv2.line(img, (x - size, y), (x + size, y), color, thickness)
    # Draw vertical line
    cv2.line(img, (x, y - size), (x, y + size), color, thickness)


def create_video(angle_list, direction_list, total_duration_minutes, start_time_ms_list, fileName):
    if len(angle_list) != len(direction_list) or len(angle_list) != len(start_time_ms_list):
        raise ValueError("The lengths of angle_list, direction_list, and start_time_ms_list must be equal.")

    # Convert the total duration to frames
    total_duration_seconds = total_duration_minutes * 60
    total_frames = int(total_duration_seconds * fps)

    # Create a video writer object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video_writer = cv2.VideoWriter(fileName, fourcc, fps, video_resolution)

    # Initial position
    center_position = (960, 540)
    current_x, current_y = center_position

    # Movement duration in frames
    movement_duration_frames = int(0.25 * fps)

    # Prepare the movement schedule
    movements = []
    cumulative_x = current_x
    cumulative_y = current_y

    for i in range(len(angle_list)):
        angle = angle_list[i]
        direction = direction_list[i]
        start_time = start_time_ms_list[i] / 1000  # Convert ms to seconds
        start_frame = int(start_time * fps)
        end_frame = start_frame + movement_duration_frames

        # Calculate delta
        if direction == 'horizontal':
            delta_x = angle * pixels_per_degree
            delta_y = 0
        elif direction == 'vertical':
            delta_x = 0
            delta_y = -angle * pixels_per_degree  # Negative because screen coordinates
        else:
            raise ValueError("Direction must be either 'horizontal' or 'vertical'.")

        target_x = cumulative_x + delta_x
        target_y = cumulative_y + delta_y

        movements.append({
            'start_frame': start_frame,
            'end_frame': end_frame,
            'from_x': cumulative_x,
            'from_y': cumulative_y,
            'to_x': target_x,
            'to_y': target_y,
            'direction': direction,
            'angle': angle
        })

        # Update cumulative position for the next movement's starting point
        cumulative_x = target_x
        cumulative_y = target_y

    # Reset current position to center for video generation
    current_x, current_y = center_position
    movement_index = 0

    # Loop through the frames and generate the video
    for frame_idx in range(total_frames):
        print(f"Generating frame {frame_idx}/{total_frames}")

        frame = background.copy()

        # Determine if the dot is moving
        if movement_index < len(movements):
            movement = movements[movement_index]
            is_moving = movement['start_frame'] <= frame_idx < movement['end_frame']
            time_to_next_movement = (movement['start_frame'] - frame_idx) / fps
        else:
            is_moving = False
            time_to_next_movement = None

        # Update dot position
        if is_moving:
            # Dot is moving
            dot_color = moving_dot_color
            progress = (frame_idx - movement['start_frame']) / movement_duration_frames
            current_x = movement['from_x'] + progress * (movement['to_x'] - movement['from_x'])
            current_y = movement['from_y'] + progress * (movement['to_y'] - movement['from_y'])
        else:
            # Dot is stationary
            dot_color = static_dot_color

        # Draw the dot
        cv2.circle(frame, (int(current_x), int(current_y)), dot_radius, dot_color, -1)

        # Always display the timer when the dot is stationary
        if not is_moving:
            # Timer showing the time until the next movement
            if movement_index < len(movements):
                next_movement_start_frame = movements[movement_index]['start_frame']
                time_remaining = int(np.ceil((next_movement_start_frame - frame_idx) / fps))
                if time_remaining < 0:
                    time_remaining = 0
            else:
                time_remaining = 0  # No more movements scheduled

            time_remaining_text = f"{time_remaining}"
            draw_centered_text(frame, time_remaining_text, (int(current_x), int(current_y)), font, font_scale, font_color, font_thickness)

            # Display arrows and cross only within 2 seconds before the next movement
            if movement_index < len(movements) and 0 < time_to_next_movement <= timer_flash_duration:
                # Determine the direction symbol
                angle = movements[movement_index]['angle']
                direction = movements[movement_index]['direction']
                if direction == 'horizontal':
                    direction_symbol = ">>" if angle > 0 else "<<"
                elif direction == 'vertical':
                    direction_symbol = "↑" if angle > 0 else "↓"
                else:
                    direction_symbol = ""

                # Draw the direction symbol below the timer
                symbol_offset = 40  # Adjust as needed
                draw_centered_text(frame, direction_symbol, (int(current_x), int(current_y) + symbol_offset), font, font_scale, font_color, font_thickness)

                # Draw the cross at the next target position
                target_x = movements[movement_index]['to_x']
                target_y = movements[movement_index]['to_y']
                cross_size = 30  # Size of the cross arms
                cross_thickness = 5  # Thickness of the cross lines (bold)
                draw_cross(frame, (int(target_x), int(target_y)), cross_size, cross_color, cross_thickness)
        else:
            # When moving, no timer, arrows, or cross are displayed
            pass

        # After movement ends, update current position and advance to next movement
        if is_moving and frame_idx == movement['end_frame'] - 1:
            # Movement has just ended
            current_x = movement['to_x']
            current_y = movement['to_y']
            movement_index += 1

        # Write the frame to the video
        video_writer.write(frame)

    # Release the video writer
    video_writer.release()
    print(f"Video has been created and saved as {fileName}.")


if __name__=='__main__':
    
    # Example usage for classical SE with given params TR and TA
    TR = 690 #ms
    TA = 3 #min
    PE75 = 75*TR
    PE90 = 90*TR
    PE105 = 105*TR
    
    # create_video(
        # angle_list=[1, 2, -1],
        # direction_list = len(angle_list)*['horizontal'],
        # total_duration_minutes=TA,
        # start_time_ms_list=[PE75, PE90, PE105],
        # fileName='02_CSE_PE_75_90_105_angles_1_2_minus1.mp4'
    # )
    
    # create_video(
        # angle_list=[2],
        # direction_list=['horizontal'],
        # total_duration_minutes=TA,
        # start_time_ms_list=[PE75],
        # fileName='03_CSE_PE_75_angle_2.mp4'
    # )
    
    
    ################# separate params for EPI training session
    interval_ms = 5000
    TA_EPI = 4
    nb_blocks = 6
    #A block corresponds to 1,2,3 then -3,-2,-1 degrees (they are specified as 1,1,1,-1,-1,-1 
    # because the script interprets angles in a RELATIVE manner )
    a=nb_blocks*[1,1,1,-1,-1,-1]
    
    #NOTE: ad-hoc fix :
    # (len(a)+2) so that start_time_ms_list has the same len as angle_list and direction_list
    create_video(
        angle_list=a,
        direction_list=len(a)*['horizontal'],
        total_duration_minutes=TA_EPI,
        start_time_ms_list=[ x for x in range(2*interval_ms,(len(a)+2)*interval_ms,interval_ms) ],
        fileName='01_EPI_training.mp4'
    )
    #################