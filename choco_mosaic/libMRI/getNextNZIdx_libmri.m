function idx = getNextNZIdx_libmri(currIdx, mosaicIn)
%GETNEXTNZIDX Summary of this function goes here
%   Detailed explanation goes here

N=size(mosaicIn,3);
isZero = true;
idx = currIdx;
while isZero

    idx = idx + 1;
    
    if idx > N
        idx = N;
        break;
    end
    
    mos = mosaicIn(:,:,idx);
    if sum(mos(:)) ~= 0
        isZero=false;
    end
end
%info getNext from e.g. 33 or more will return 36 (last index) ie index of
%a 0 slice (it could not find a sum(mos(:)) ~= 0 after searching
%which is what we want in fact
end

