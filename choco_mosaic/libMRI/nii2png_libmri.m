function nii2png_libmri(inputDir,anatomicalPlane,outputDir_opt)
%NII2PNG_LIBMRI Summary of this function goes here
%   optional arg outputDir_opt: if  not given a default one base on the
%   input dir is created (this is most likely the one I will use for
%   convenicnece)
% 
% anatomicalPlane : 'ax' or 'sag'  ('cor' not implemented)
%ex call : nii2png_libmri('C:\MRIDATA\ts_data\s2_interpRef\','ax')
%
% or (preffered) 2 calls: 
%nii2png_libmri('C:\MRIDATA\ts_data\s2\','ax','C:\MRIDATA\ts_data\s2\nii2png\ax\')
%nii2png_libmri('C:\MRIDATA\ts_data\s2\','sag','C:\MRIDATA\ts_data\s2\nii2png\sag\')

nTs = 3;

if nargin < 3
    outputDir_opt = fullfile(inputDir,'nii2png');
end
mkdir(outputDir_opt);

inputDirRL = fullfile(inputDir,'_RL');
inputDirUD = fullfile(inputDir,'_UD');

ts_RL = dir([inputDirRL '/*_RL']);%both / and \ seem to work
ts_RL = {ts_RL.name};

ts_UD = dir([inputDirUD '/*_UD']);
ts_UD = {ts_UD.name};

for moDir = 0:1
    if moDir == 0
        currTsDir = ts_RL;
        prefixDir = '_RL';
    else
        currTsDir = ts_UD;
        prefixDir = '_UD';
    end

    for tsIdx = 1:nTs
        currOutDir = fullfile(outputDir_opt,prefixDir,['ts_' num2str(tsIdx)])
        mkdir(currOutDir)
        for i=1:length(dir(fullfile(inputDir,prefixDir,currTsDir{tsIdx},'*.nii')))%should be around 645
            currFn = ['ts_' insertZeros_libmri(i) num2str(i) '.nii']
            x = niftiread(fullfile(inputDir,prefixDir,currTsDir{tsIdx},currFn));
            if strcmpi(anatomicalPlane,'ax')
                reMosaicTensor = double( reMosaic_libmri(x) );
            elseif strcmpi(anatomicalPlane,'sag')
                reMosaicTensor = double( reMosaicSag_libmri(x) );
            else
                error('>>>> error not implemented <<<<<');
            end
            reMosaicTensor = (reMosaicTensor-min(reMosaicTensor(:)))/(max(reMosaicTensor(:))-min(reMosaicTensor(:)));
            outputFn = fullfile(currOutDir,[currFn(1:end-4) '.png'])
            imwrite(reMosaicTensor,outputFn);
        end
    end
end


end

