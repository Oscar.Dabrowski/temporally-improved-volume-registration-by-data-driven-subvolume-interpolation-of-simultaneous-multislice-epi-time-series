function u = upsample3ts_libmri(ts1_rotParams,ts2_rotParams,ts3_rotParams)

    N=length(ts1_rotParams);
    j=1;
    for i=1:N
        u(j) = ts1_rotParams(i);
        u(j+1) = ts2_rotParams(i);
        u(j+2) = ts3_rotParams(i);
        j=j+3;
    end
end