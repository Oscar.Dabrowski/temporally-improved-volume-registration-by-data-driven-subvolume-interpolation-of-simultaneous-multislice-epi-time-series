function mosaic_out = splitMosaic12_withInterp_libmri(mosaic_in,sortedMosaicIdx,n_slices_per_mosaic,n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols,mtsDir,nTs,doInterp, interpType, ts_ref, tform_forward, tform_backward)
%SPLITMOSAIC Summary of this function goes here
%   splits a Siemens mosaic 6x6 into 9  mosaics of 6x6 with only 4 nonzero
%   slices (64x64) corresponding to simultaneously acquired slices
% sortedMosaicIdx : vector of indexes corresponding to the temporal order
% in which slices are aquired (each group of 4 subsequent indexes were
% acquired simultaneously)
%
%mosaic_out: 4D tensor of shape 64x64x36x9, i.e. 9 elements of each of 9
%times series i.e. each 'element' of each time series is a 64x64x36 mosaic
%(volume) with 4 non-zero slices (the other 32 slices are padded with
%zeros) this is so that they may be relaigned with the first slice (noMo)
%which will be full (6x6 nonzero slices) and is the reference for
%realignment

mosaic_out = zeros(n_slice_rows,n_slice_cols,n_row_blocks*n_row_blocks,nTs);
k=1;
for i=0:n_row_blocks-1
    for j=0:n_col_blocks-1
        curr_row_start = i*n_slice_rows+1;
        curr_col_start = j*n_slice_cols+1;
        mosaic_out_full(:,:,k) = mosaic_in(curr_row_start:curr_row_start+n_slice_rows-1, curr_col_start:curr_col_start+n_slice_cols-1) ;
        k = k+1;
    end
end

k=1;
nSlTs = (n_row_blocks*n_row_blocks)/nTs;%should give 12 for nts=3 i.e. 12 slices per ts for 36 slices total
for i=1:nTs %3 time series (ts)
    for j=1:nSlTs %12 slices per ts
        sliceIdx = sortedMosaicIdx(k);
        
        % if thresh: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %mof = mosaic_out_full(:,:,sliceIdx);
        %mosaic_out(:,:,sliceIdx,i) = uint16( double( mof>0.1*max(mof(:)) )); %mosaic_out_full(:,:,sliceIdx);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        mosaic_out(:,:,sliceIdx,i) = mosaic_out_full(:,:,sliceIdx);
        k=k+1;
    end
    if doInterp
        if strcmp(interpType,'linear3d')
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!! proably never gonna use this one
            %again (we re only useing pairwise tforms
            mosaic_out(:,:,:,i) = doInterpMosaic_libmri(mosaic_out(:,:,:,i),i,mtsDir);%i==tsidx
            
            error('>>>>>>>>> Should never arrive here');
        elseif strcmp(interpType,'morpho')
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %not implemented getTsInfo_libmri : since most likely never
            %gonna use it again
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            %in this case we will be interested by only mask3d since we
            %decided to stay in binary for all morpho operations (no
            %mask applied to the grayscale mosaic )
            tsInfo = getTsInfo_libmri(i);%needs to know time series (ts) index
            %refslice will either be RL or UD depending on what is
            %given as input to splitMosaic...
            %takes ar arg ts_ref of the first slice for the current time
            %series (i)
            
            
            %% *******************************************
            %                 fullFirstMosaicDataDir_RL = 'C:\MRIDATA\ts_data\s2_3ts_12slPerVol_v2_spmMeta\_RL\ts1_subject2_RL_orig\';
            %                 full_mosaic_RL_file = fullfile(fullFirstMosaicDataDir_RL,'ts_000001.nii');
            %                 mosaic_full_3D = niftiread(full_mosaic_RL_file);
            %
            %                 tmp_fullMosaic2D = reMosaic(mosaic_full_3D,6);
            %                 [~, mosaic_full_3D_th] = customThresh(tmp_fullMosaic2D);
            
            
            %% *********************************************
            
            %strelInfo = getStrelInfo(mosaic_full_3D_th);%recomputes the best strels from reference slice (not many of them so is fast)
            
            strelInfo = getStrelInfo_libmri( logical(ts_ref(:,:,:,i,1)) );%recomputes the best strels from reference slice (not many of them so is fast)
            [mask_3d, mosaic_out_gray_masked_3d, mosaic_out_gray_3d] = recMosaicMorpho_libmri(mosaic_out(:,:,:,i),tsInfo,strelInfo);
            mosaic_out(:,:,:,i) = mask_3d;
            
            error('>>>>>>>>> Should never arrive here');
        elseif strcmp(interpType,'morphoMask')
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %not implemented getTsInfo_libmri : since most likely never
            %gonna use it again
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            tsInfo = getTsInfo_libmri(i);
            strelInfo = getStrelInfo_libmri( logical(ts_ref(:,:,:,i,1)) );%recomputes the best strels from reference slice (not many of them so is fast)
            [mask_3d, mosaic_out_gray_masked_3d, mosaic_out_gray_3d] = recMosaicMorpho_libmri(mosaic_out(:,:,:,i),tsInfo,strelInfo);
            mosaic_out(:,:,:,i) = mosaic_out_gray_masked_3d;
            error('>>>>>>>>> Should never arrive here');
        elseif strcmp(interpType,'pairwiseTform')
            disp('>>>>>>>> pairwiseTform v1');
            pairwiseTsStruct = getPairwiseTsMatrix_libmri(i);
            mosaic_out(:,:,:,i)= recMosaicPairwiseTform_libmri(mosaic_out(:,:,:,i),pairwiseTsStruct,tform_forward,tform_backward);
            %mosaic3d_out = recMosaicPairwiseTform(mosaic3d_partial_in,pairwiseTsStruct,tform_forward,tform_backward);
            
        elseif strcmp(interpType,'pairwiseTform_v2')
            %%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> only gonna use this one (
            %%pairwise v2)
            %pairwiseTsStruct = getPairwiseTsMatrix_libmri(i);
            %REM i : ts index
            mosaic_out(:,:,:,i)= recMosaicPairwiseTform_v2_libmri(mosaic_out(:,:,:,i),i,tform_forward,tform_backward, n_slices_per_mosaic,n_slice_rows,n_slice_cols);
              %wrong way to normalize?
%             if doNormalizeTs
%                 mos_tmp = mosaic_out(:,:,:,i);
%                 mosaic_out(:,:,:,i) = ( mos_tmp-min(mos_tmp(:)) )/( max(mos_tmp(:)) - min(mos_tmp(:)) );
%             end
        end
    end
end

%     %for i=1:3 %3 time series (ts)
%         for j=1:36 %12 slices per ts
%             %sliceIdx = sortedMosaicIdx(k);
%             mosaic_out(:,:,j,1) = mosaic_out_full(:,:,j);
%             %k=k+1;
%         end
%     %end
end

