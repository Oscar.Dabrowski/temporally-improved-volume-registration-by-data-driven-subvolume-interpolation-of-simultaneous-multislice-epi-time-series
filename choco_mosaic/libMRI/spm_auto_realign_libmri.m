function spm_auto_realign_libmri(pathToNIIFiles, currDir)
%SPM_AUTO_REALIGN Summary of this function goes here
%   Detailed explanation goes here
cd(pathToNIIFiles);
P = spm_select('List',pathToNIIFiles,'\.nii$');%also works with regex: spm_select('List',pathToDICOMFiles,'.*.nii$');
spm_realign(P)
cd(currDir);%back to dir from which script was called
end

