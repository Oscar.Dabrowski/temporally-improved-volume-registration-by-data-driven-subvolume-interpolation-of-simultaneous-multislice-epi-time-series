function z = insertZeros_libmri(i)
%INSERTZEROS Summary of this function goes here
max_digits = 6,
n = numel(num2str(i));
z = strrep(num2str(zeros(1,max_digits-n)),' ','');
end

