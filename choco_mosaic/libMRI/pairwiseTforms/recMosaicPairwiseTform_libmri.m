function mosaic3d_out = recMosaicPairwiseTform_libmri(mosaic3d_partial_in,pairwiseTsStruct,tforms_fwd,tforms_bkwd)
%RECMOSAICPAIRWISETFORM Summary of this function goes here
%   Detailed explanation goes here

%mosaic_in_partial_2D = double( reMosaic( mosaic_in_partial3D,6) )/(2^16);

mosaic3d_out  = mosaic3d_partial_in;%zeros(size(mosaic3d_partial_in));

for i=1:36
    if ~isempty( pairwiseTsStruct{i} )
        tformDir = pairwiseTsStruct{i}.tformDir;
        baseIdx = pairwiseTsStruct{i}.baseIdx;
        tformIdx = pairwiseTsStruct{i}.tformIdx;

        curr_tform = eye(3);

        if strcmpi(tformDir,'forward')
            for j=1:length(tformIdx)
                %tforms_list{j} = tforms_fwd{tformIdx(j)};
                curr_tform = curr_tform*tforms_fwd{tformIdx(j)}.T;%matrix product (transform composition)
            end
        else%backward tforms
            for j=1:length(tformIdx)
                curr_tform = curr_tform*tforms_bkwd{tformIdx(j)}.T;%matrix product (transform composition)
            end
        end
        %imwarp(moving,tform_forward{i},'OutputView',imref2d(size(fixed)));
        mosaic3d_out(:,:,i) = imwarp(mosaic3d_partial_in(:,:,baseIdx),affine2d(curr_tform),'OutputView',imref2d([64 64]));
    end
end

max_mosaic3d_q0_99 = quantile(mosaic3d_out(:),0.99);
mosaic3d_out = (mosaic3d_out-min(mosaic3d_out(:)))/(max_mosaic3d_q0_99-min(mosaic3d_out(:)));

end

