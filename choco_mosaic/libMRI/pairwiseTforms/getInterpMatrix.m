function interpMatrix = getInterpMatrix(tsIdx)
%GETINTERPMATRIX Summary of this function goes here
%   get linear interp coeff matrix associated with given time series (ts)
%   idx (e.g. ts3)
%these matrices are compatible with a 6x6 mosaic, they must be changed
%(possibly by just nearest neighbour interpolation ?) for a different mosaic shape
%hardcoded matrices (manually designed for each of the 3 time series)

%REM these are slightly different than the MTSX CSVs
ts1Mtx = [0,-1,0.5,-1,0.5,-1 ;
        0.9,0.63,0.37,0.1,-1,0.5 ;
        -1,0.5,-1,0.9,0.63,0.37 ;
        0.1,-1,0.5,-1,0.5,-1 ;
        0.9,0.63,0.37,0.1,-1,0.5 ;
        -1,0.5,-1,1,1,1];
    
ts2Mtx = [ -1,0.5,-1,0.9,0.63,0.37 ;
            0.1,-1,0.5,-1,0.5,-1 ;
            0.9,0.63,0.37,0.1,-1,0.5 ;
            -1,0.5,-1,0.9,0.63,0.37 ;
            0.1,-1,0.5,-1,0.5,-1 ;
            0.9,0.63,0.37,0.1,-1,1];
        
ts3Mtx = [ 0,0,0,0,-1,0.5 ;
            -1,0.5,-1,0.9,0.63,0.37 ;
            0.1,-1,0.5,-1,0.5,-1 ;
            0.9,0.63,0.37,0.1,-1,0.5 ;
            -1,0.5,-1,0.9,0.63,0.37 ;
            0.1,-1,0.5,-1,1,1];
        
switch tsIdx
    case 1
        interpMatrix = ts1Mtx;
    case 2
        interpMatrix = ts2Mtx;
    case 3
        interpMatrix = ts3Mtx;
end

end

