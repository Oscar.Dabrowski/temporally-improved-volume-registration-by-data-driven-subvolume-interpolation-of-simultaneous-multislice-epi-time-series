function mosaic3d_out = recMosaicPairwiseTform_v2_libmri(mosaic3d_partial_in,tsIdx,tforms_fwd,tforms_bkwd,n_slices_per_mosaic,n_slice_rows,n_slice_cols )
%RECMOSAICPAIRWISETFORM Summary of this function goes here
%REM: CAREFUL: 2nd arg is tsIdx (not tsStruct) ! (w.r.t recMosPairwise...
% ("v1" i.e. recMosaicPairwiseTform_libmri(...)
%
%   This v2 uses both pairwise tforms and linear interp to be more accurate
%   (better "blending")

%mosaic_in_partial_2D = double( reMosaic( mosaic_in_partial3D,6) )/(2^16);

idxAlphaMax = 4;%cf linspace(0.1,0.9,4) the maximal beta idx  is 4 as it is harcoded currently
mosaic3d_out  = mosaic3d_partial_in;%zeros(size(mosaic3d_partial_in));

interpMatrix = getInterpMatrix(tsIdx);%matrices are hardcoded: they should be changed if a different mosaic shape is used
nSlices = size(mosaic3d_partial_in,3);%should be 36

if nSlices ~= n_slices_per_mosaic
    error('>>>>>>>>>>>>>>>>> nSLices != 36 -> check recMosaicPairwiseTform_v2_libmri <<<<<<<<<<<');
end

for i=1:nSlices
    
    [idxRows idxCols] = ind2sub(size(interpMatrix),i);
    alpha = interpMatrix(idxCols, idxRows) ;%swap idx because of column major indexing
    beta = 1-alpha;
    if alpha ~= -1
        tform_nxtSlice = eye(3);
        tform_prevSlice = eye(3);
        nxtSliceIdx = getNextNZIdx_libmri(i,mosaic3d_partial_in);
        prevSliceIdx = getPrevNZIdx_libmri(i,mosaic3d_partial_in);
        
        %special cases for alpha=0 or alpha=1
        if alpha == 0
            for j=nxtSliceIdx:-1:i
                tform_nxtSlice = tform_nxtSlice*tforms_bkwd{j}.T;
            end
            %alpha=0 beta=1
            mosaic3d_out(:,:,i) =  imwarp(mosaic3d_partial_in(:,:,nxtSliceIdx),affine2d(tform_nxtSlice),'OutputView',imref2d([n_slice_rows n_slice_cols]));
        elseif alpha == 1
            for j=prevSliceIdx:i-1
                %tform_nxtSlice = tform_nxtSlice*tforms_bkwd{j}.T;
                tform_prevSlice = tform_prevSlice*tforms_fwd{j}.T;
            end
            %alpha=1, beta=0
            mosaic3d_out(:,:,i) = imwarp(mosaic3d_partial_in(:,:,prevSliceIdx),affine2d(tform_prevSlice),'OutputView',imref2d([n_slice_rows n_slice_cols]));
        elseif alpha == 0.5
            imwarp_forward = imwarp(mosaic3d_partial_in(:,:,prevSliceIdx),affine2d(tforms_fwd{prevSliceIdx}.T),'OutputView',imref2d([n_slice_rows n_slice_cols]));
            %backward tform applied on NEXT slice ("go backward from next")
            imwarp_backward = imwarp(mosaic3d_partial_in(:,:,nxtSliceIdx),affine2d(tforms_bkwd{nxtSliceIdx}.T),'OutputView',imref2d([n_slice_rows n_slice_cols]));
            mosaic3d_out(:,:,i) = alpha*imwarp_forward + beta*imwarp_backward;
        else
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %REM alpha : %prev, beta: %next
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %REM if alpha is not 0 or 1 then it must be one of the possible
            %props: 0.1 or 0.37 or 0.63 or 0.9 (i.e., linspace(0.1,0.9,4)
            
            %returns the index corresponding to the proportion in the table of
            %proportions (props) e.g. 0.1 -> 1, 0.37 -> 2, ...
            alphaIdx = prop2idx_libmri(alpha);
            
            for j=prevSliceIdx:prevSliceIdx+alphaIdx
                %if j<36
                    tform_prevSlice = tform_prevSlice*tforms_fwd{j}.T;%matrix product (transform composition)
                %end
            end
            for j=nxtSliceIdx:-1:(nxtSliceIdx-(idxAlphaMax-alphaIdx))
                %if j <36
                    tform_nxtSlice = tform_nxtSlice*tforms_bkwd{j}.T;%matrix product (transform composition)
                %end
            end
            %forward applied on PREV slice (go forward "from" prev)
            imwarp_forward = imwarp(mosaic3d_partial_in(:,:,prevSliceIdx),affine2d(tform_prevSlice),'OutputView',imref2d([n_slice_rows n_slice_cols]));
            %backward tform applied on NEXT slice ("go backward from next")
            imwarp_backward = imwarp(mosaic3d_partial_in(:,:,nxtSliceIdx),affine2d(tform_nxtSlice),'OutputView',imref2d([n_slice_rows n_slice_cols]));
            mosaic3d_out(:,:,i) = alpha*imwarp_forward + beta*imwarp_backward;
        end

    end
end
max_mosaic3d_q0_99 = quantile(mosaic3d_out(:),0.95);
mosaic3d_out = (mosaic3d_out-min(mosaic3d_out(:)))/(max_mosaic3d_q0_99-min(mosaic3d_out(:)));

end

