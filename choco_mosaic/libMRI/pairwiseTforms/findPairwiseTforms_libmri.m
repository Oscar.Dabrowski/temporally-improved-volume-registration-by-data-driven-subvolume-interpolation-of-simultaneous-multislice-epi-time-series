function [tform_forward, tform_backward]  = findPairwiseTforms_libmri(refNoMo)
%FINDPAIRWISETFORMS Summary of this function goes here
%   refNomo: (string) path to dicom file containing reference nomotion
%   volume for the given subject (with subject ID : subjId (int))
%
%we isolated nomo ref volumes breforehand to be sure not to mix them with
%other data (e.g. realigned with spm ...)

mosaic_full_3D = unMosaic_libmri(dicomread(refNoMo));

[rows cols slices]  = size(mosaic_full_3D)

tform_forward = cell(1,slices);
tform_backward = cell(1,slices);

%cf matlab doc
% The regular step gradient descent optimization adjusts the transformation parameters so that the optimization follows the gradient of the image similarity metric in the direction of the extrema. 
%It uses constant length steps along the gradient between computations until the gradient changes direction. 
%At this point, the step length is reduced based on the RelaxationFactor, which halves the step length by default.
% https://www.mathworks.com/help/images/ref/registration.optimizer.regularstepgradientdescent.html

[optimizer, metric] = imregconfig('monomodal');%or multimodal
optimizer.MaximumStepLength = optimizer.MaximumStepLength/50;
%optional fine-tuning

for i=1:slices-1
    %% reg i to i+1
    moving = mosaic_full_3D(:,:,i);
    fixed = mosaic_full_3D(:,:,i+1);
    tform_forward{i} = imregtform(moving, fixed, 'affine', optimizer, metric);
    %tform_forward{i}.T
end

[optimizer, metric] = imregconfig('monomodal');%need to re-init optimizer?
optimizer.MaximumStepLength = optimizer.MaximumStepLength/50;

for i=1:slices-1
    %% reg i+1 to i
    moving = mosaic_full_3D(:,:,i+1);
    fixed = mosaic_full_3D(:,:,i);
    tform_backward{i} = imregtform(moving, fixed, 'affine', optimizer, metric);
    %tform_forward{i}.T
end

%% harcoded for last slice
tform_backward{36} = affine2d(eye(3));
tform_forward{36} =  affine2d(eye(3));
end