function interpMosaic = doInterpMosaic_libmri(mosaicIn,tsIdx,mtsDir)
%DOINTERPMOSAIC Summary of this function goes here
%   interpolation with variable coefs
M_ts = csvread(fullfile(mtsDir,['MTS' num2str(tsIdx) '.csv']));
interpMosaic = double( mosaicIn );
rowSize = size(mosaicIn,1);
colSize = size(mosaicIn,2);
for i=1:numel(M_ts)
    [idxx idxy]=ind2sub(size(M_ts),i);
    %swap x and y because i counted as row-major in my OneNote (diagrams)
    %to build M_ts
    if M_ts(idxy,idxx) ~= -1
        if i ~= numel(M_ts) %if i == numel(M_ts) (i.e. 36) then we leave it to zero becuase last slice is always almost zero 
            %swap x and y because i counted as row-major in my OneNote (diagrams)
            %to build M_ts
            alpha = M_ts(idxy,idxx);
            beta = 1-alpha;
            alpha_idx = getPrevNZIdx_libmri(i,mosaicIn);
            beta_idx = getNextNZIdx_libmri(i,mosaicIn);
            if alpha_idx >= 1 && alpha_idx <= numel(M_ts)
                tmp_im_alpha = rescale( interpMosaic(:,:,alpha_idx) );
                alphaPrevSl = alpha*tmp_im_alpha;%interpMosaic(:,:,alpha_idx);
            else
                alphaPrevSl = zeros(rowSize,colSize);
            end
            if beta_idx >= 1 && beta_idx <= numel(M_ts)
                tmp_im_beta = rescale( interpMosaic(:,:,beta_idx) );
                betaNextSl = beta*tmp_im_beta;
            else
                betaNextSl = zeros(rowSize,colSize);
            end
            %recast to uint16 because it was the orig datatype to comply with
            %nii metadata
            interpSlice = rescale(alphaPrevSl+betaNextSl);
            interpMosaic(:,:,i) = uint16( round( (interpSlice)*2^16 ) );
        end
    else
        %process non interpolated slice (existing data)
        %rescale (but do not change) to keep same scale as for other (the interpolated) slices
        interpMosaic(:,:,i) =  uint16( round( rescale( double(interpMosaic(:,:,i)) )*2^16 ) );
    end
end

end

