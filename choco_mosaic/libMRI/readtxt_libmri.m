function outputTxt = readtxt_libmri(fn)
%READTEXTF Summary of this function goes here
%   Detailed explanation goes here

fileID = fopen(fn);
outputTxt = textscan(fileID,'%s');
fclose(fileID);

%outside fun: access elem by outputTxt{LineNum}
outputTxt = outputTxt{1};

end

