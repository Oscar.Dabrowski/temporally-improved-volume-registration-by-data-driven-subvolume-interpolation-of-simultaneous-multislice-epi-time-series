function mosaic2d = reMosaicSag_libmri(mosaic3d)
%VIEWMOSAICSAG Summary of this function goes here
%   Detailed explanation goes here

%REM hardcoded for excpected input tensor (mosaic3d) of shape 64x64x36
n_blocks_per_rowOrCol = 8;%in sagittal view : 8x8 matrix of 64x36 images

%sliceResol_rows = size(mosaic3d,1); sliceResol_cols = size(mosaic3d,3);%!!! 64 and 36 !!! for sagittal view
mosaic2d = [];% output size will be : n_slab_rows_out,n_slab_cols_out,1,1);
mosaic2d_row = [];
for i=1:n_blocks_per_rowOrCol
    for j=1:n_blocks_per_rowOrCol
        %concat blocks horizontally to build a row
        idxLin = sub2ind([n_blocks_per_rowOrCol, n_blocks_per_rowOrCol],j,i);
        mosaic2d_row = horzcat(mosaic2d_row, squeeze(mosaic3d(:,idxLin,:)) );
    end
    %careful vertcat or horzcat 
    mosaic2d = vertcat(mosaic2d,squeeze(mosaic2d_row));%concat rows vertically
    mosaic2d_row = [];%reinitialize not to accumulate with prev row
end

%to view it it 'correctly'
mosaic2d = flipud(mosaic2d.');
end

