function idx = getPrevNZIdx_libmri(currIdx, mosaicIn)
%GETNEXTNZIDX Summary of this function goes here
%   Detailed explanation goes here

N=size(mosaicIn,3);
isZero = true;
idx = currIdx;
while isZero

    idx = idx - 1;
    
    if idx <= 0
        idx = 1;
        break;
    end
    
    mos = mosaicIn(:,:,idx);
    if sum(mos(:)) ~= 0
        isZero=false;
    end
end

end