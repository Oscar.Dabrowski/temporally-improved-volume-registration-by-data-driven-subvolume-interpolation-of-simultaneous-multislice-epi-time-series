function V = readDicomFromAcqNum_libmri(dicomFileList, chkZeros_opt)
%READDICOMFROMACQ Summary of this function goes here
%   this function takes a list of dicom filenames and outputs a tensor
%with the files data (volumes) placed in correct acquisition order (metadata field
%AcquisitionNumber) because in some situations the file name is misleading
%e.g. 0005.dcm is later than 0099.dcm 
%
%
%example of how to prroduce a correct fileList, from e.g.:
% dicomFileList = dir(['C:\MRIDATA\for_ts_s1_s6_dicom_nii_clean\s5\dicom_RL_orig\*.dcm'])
%
% dirs = {dicomFileList.folder};
% names = {dicomFileList.name};
% %fullfile is able to pair them correctly
% dicomFileList = fullfile(dirs,names);

%isa(...,'string') is excpeted to be a string array in fact
assert(isa(dicomFileList,'cell') || isa(dicomFileList,'string') ,'dicomFileList is type %s, not cell or string array.',class(dicomFileList));
assert(~isempty(dicomFileList),'dicomFileList is empty!');

[M N] = size(dicomread(dicomFileList{1}));
L = length(dicomFileList);
V = zeros(M,N,1,L);
size(V)

for i=1:L
    dInfo = dicominfo(dicomFileList{i});
    acqNum = dInfo.AcquisitionNumber;
    V(:,:,1,acqNum) = dicomread(dicomFileList{i});
end

if nargin > 1
    if chkZeros_opt
        for i=1:size(V,4)
            v = V(:,:,:,i);
            idx(i) = ( sum(v(:)) == 0 );
        end
        %assert(isa(idx,'logical'),'idx must be of type logical');
        V(:,:,:,idx) = [];%remove full zero volumes
    end
end

%debug check (correct order in implay)
% for i=1:644
% 	TMPi =  double(V(:,:,1,i));
% 	q95 = quantile(TMPi(:),0.95);
% 	TMP(:,:,i) = (TMPi-min(TMPi(:)))/(q95-min(TMPi(:)));
% end
% implay(TMP)
end

