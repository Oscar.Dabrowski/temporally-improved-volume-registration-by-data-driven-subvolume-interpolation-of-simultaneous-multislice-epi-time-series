function [t_xyz, rot_xyz] = getRigidBodyParams_ts_libmri(fname, f, dstDir)
%GETRIGIDBODYPARAMS_TS_LIBMRI Summary of this function goes here
%   Detailed explanation goes here
param_matrix = dlmread(f);%should be a Nx6 matrix

lineWidthSz = 2;

fig=figure('Position', get(0, 'Screensize'));

hold on;grid on;
subplot(211);
plot(param_matrix(:,1),'--r','linewidth',lineWidthSz);hold on;grid on;
plot(param_matrix(:,2),'--g','linewidth',lineWidthSz);hold on;grid on;
plot(param_matrix(:,3),'--b','linewidth',lineWidthSz);hold on;grid on;
xlabel('EPI volume');ylabel('translation (mm)');
legend('tx','ty','tz');
hold off;

subplot(212);
plot(rad2deg(param_matrix(:,4)),'r','linewidth',lineWidthSz);hold on;grid on;
plot(rad2deg(param_matrix(:,5)),'g','linewidth',lineWidthSz);hold on;grid on;
plot(rad2deg(param_matrix(:,6)),'b','linewidth',lineWidthSz);hold on;grid on;
xlabel('EPI volume');ylabel('angle (degrees)');
legend('pitch','roll','yaw');
hold off;

saveas(fig, fullfile(dstDir,[fname '_rigidBodyParams_est_spm_3ts.png']));

t_xyz = param_matrix(:,1:3);
rot_xyz = rad2deg(param_matrix(:,4:6));
end

