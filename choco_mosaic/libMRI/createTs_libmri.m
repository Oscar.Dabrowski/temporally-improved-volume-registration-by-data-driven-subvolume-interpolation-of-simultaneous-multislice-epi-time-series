function resDirList = createTs_libmri(sortedMosaicIdx,origNiiFilesDir,dicomDir,doWrite,n_ts,dataDir,moDirStr,subjIdx,mtsDir, doInterp, interpType, tform_forward, tform_backward, n_slab_rows, n_slab_cols,n_blocks,n_slice_rows,n_slice_cols,n_slices_per_mosaic,block_size )
%CREATETS Summary of this function goes here
%   creats ALL the time series for 1 subject

%check for full zeroed volumes which may happen due to annoying unordered
%file names in the new 'Kheops' system  ? due to anonymization procedures ?
chkZeros_opt = true;

if nargin <= 13
    %default params for our proof of concept
    n_slab_rows = 384;
    n_slab_cols = 384;
    block_size = 6;
    n_blocks = block_size*block_size;
    n_slice_rows = 64;%64*6=384
    n_slice_cols = 64;
    %We have the value n_slices from the 'NumberOfImagesInMosaic' field of the Siemens private (CSA) header
    %-> BUT does NOT seem to exist e.g.: dicominfo(coll.Filenames{1}{1})
    n_slices_per_mosaic = 36;
end

subjIdxStr = ['s' num2str(subjIdx) ]
         
coll = dicomCollection(dicomDir);

%the tests for subjectID > 4 was specific to some data naming format
%convention problems with our data but should not be of any concern for the
%current demo since we provided data for our "subject #2"
if subjIdx > 4
    fprintf('\n >>>>>> reading DICOM data with readDicomFromAcqNum_libmri for %d > 4 ... \n',subjIdx); 
    V = readDicomFromAcqNum_libmri(coll.Filenames{1}, chkZeros_opt);%coll.Filenames{1} isa string array
else
    V = dicomreadVolume(coll);%should be 384   384     1   642
end

size(V)
n_mosaics = size(V,4)

baseScriptDir = pwd

%% variables (or constants)



n_row_blocks = ceil(sqrt(n_slices_per_mosaic));
n_col_blocks = n_row_blocks;
%% 

ts = uint16( zeros(n_slice_rows,n_slice_cols,n_slices_per_mosaic,n_ts,n_mosaics) );

%refMosaic =  double( extract_single_mosaic(V(:,:,1,1),n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols) );
%keep orig datatype (uint16) for compat with orig nii metadata ; we can
%alsaways convert later ...
refMosaic =   unMosaic_libmri(V(:,:,1,1));%extract_single_mosaic(V(:,:,1,1),n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols) ;


%refMosaics =  double( extract_single_mosaic(V(:,:,1,1),n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols) );


for i=1:n_ts
    
    % !!! BINARIZED VERSION !!!
    %ts(:,:,:,i,1) =  uint16( double( refMosaic>0.1*max(refMosaic(:)) ));
    if strcmp(interpType,'morpho')%thresh 1st ref slice for morpho
        [~,refMosaic_th] = customThresh_libmri(reMosaic_libmri(refMosaic), n_slice_rows, n_slice_cols, block_size );
        ts(:,:,:,i,1) = uint16(refMosaic_th);
        %%%%%%%%%% left "just in cas" but not used anymore
        error('Should not arrive here (morpho) ');
    elseif strcmp(interpType,'morphoMask')
        ts(:,:,:,i,1) = refMosaic;%grayscale for morphoMask
        %%%%%%%%left "just in cas" but not used anymore
        error('Should not arrive here (morphoMask) ');
    else
        %ref vol: apply same quantile normalization as all the other
        %volumes i.e. w.r.t. 99th quantile
        refMosaic = double(refMosaic);
        max_refMosaic_q0_99 = quantile(refMosaic(:),0.95);
        refMosaic = (refMosaic-min(refMosaic(:)))/(max_refMosaic_q0_99-min(refMosaic(:)));
        ts(:,:,:,i,1) =  uint16(refMosaic* 2^16);%permute(refMosaic,[2 1 3]);%transposed
    end
end

for i=1:n_mosaics%i=2:n_mosaics %i=1:n_mosaics%%%%%%%%%%%%%%%%%i=2:n_mosaics
    mosaic = V(:,:,1,i);%initial full (about) 645 volumes (mosaics) 
    %mosaic_out = splitMosaic(mosaic,sortedMosaicIdx,n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols);
    mosaic_out = splitMosaic12_withInterp_libmri(mosaic,sortedMosaicIdx,n_slices_per_mosaic, n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols,mtsDir,n_ts, doInterp, interpType,ts, tform_forward, tform_backward );
    ts(:,:,:,:,i) = uint16( mosaic_out * 2^16);
    %[outputEven, outputOdd] = sl(mosaic,n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols);
    i
end



if subjIdx > 4
    orig_nii_file_list = dir([origNiiFilesDir '*.nii' ]);
    orig_nii_file_list = {orig_nii_file_list.name};
end

if subjIdx > 4
    nbf = length(orig_nii_file_list)
else
    nbf = n_mosaics;
end
    
for ts_idx=1:n_ts
    %nifiti output
    ts_name = ['ts' num2str(ts_idx) '_s' num2str(subjIdx) moDirStr]
    resDir = fullfile(dataDir,subjIdxStr,moDirStr,ts_name);
    mkdir(resDir);
    resDirList{ts_idx} = resDir;
    for i=1:nbf
        %fn = fullfile(resDir,['ts_' insertZeros(i) num2str(i)])
        fn = [ 'ts_' insertZeros_libmri(i) num2str(i) '.nii' ]
        
        
        if subjIdx > 4
            orig_nifti_file = fullfile(origNiiFilesDir, orig_nii_file_list{i})
        else
            orig_nifti_file = dir([origNiiFilesDir '*' insertZeros_libmri(i) num2str(i) '*.nii']);
            orig_nifti_file = fullfile(orig_nifti_file.folder, orig_nifti_file.name)
        end
        
%         orig_nifti_file = dir([origNiiFilesDir '*' insertZeros_libmri(i) num2str(i) '*.nii']);
%         orig_nifti_file = fullfile(orig_nifti_file.folder, orig_nifti_file.name)
        
        
        %https://stackoverflow.com/questions/47313686/copy-one-nifti-header-to-another
        HeaderInfo = spm_vol(orig_nifti_file);%niftiinfo(orig_nifti_file);
        %orig_nifti_file = (end-12:end-7) 
        if doWrite
            cd(resDir);%spm_vol seems to need to be in same dir as output
            HeaderInfo.fname = fn;
            HeaderInfo.private.dat.fname = HeaderInfo.fname;
            spm_write_vol(HeaderInfo,ts(:,:,:,ts_idx,i)); 
            %niftiwrite(ts(:,:,:,ts_idx,i),fn,niftiMetadata);
            cd(baseScriptDir);%back to current dir
        end
    end
    copyfile(resDir,[resDir '_orig']);
end

end