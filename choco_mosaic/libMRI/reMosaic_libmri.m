function reMosaicTensor = reMosaic_libmri(deMosaicTensor,n_blocks_per_rowOrCol)
%REMOSAICIZE Summary of this function goes here
%   Detailed explanation goes here
if nargin == 1
    n_blocks_per_rowOrCol = 6;
end
sliceResol = size(deMosaicTensor,1);%should be 64 in our context
reMosaicTensor = [];% output size will be : n_slab_rows_out,n_slab_cols_out,1,1);
reMosaicTensor_row = [];
for i=1:n_blocks_per_rowOrCol
    for j=1:n_blocks_per_rowOrCol
        %concat blocks horizontally to build a row
        idxLin = sub2ind([n_blocks_per_rowOrCol, n_blocks_per_rowOrCol],j,i);
        if idxLin <= size(deMosaicTensor,3)
            reMosaicTensor_row = horzcat(reMosaicTensor_row,deMosaicTensor(:,:,idxLin));
        else%add zero stuffed images
            reMosaicTensor_row = horzcat(reMosaicTensor_row,zeros(sliceResol,sliceResol));
        end
    end
    reMosaicTensor = vertcat(reMosaicTensor,reMosaicTensor_row);%concat rows vertically
    reMosaicTensor_row = [];%reinitialize not to accumulate with prev row
end
end

