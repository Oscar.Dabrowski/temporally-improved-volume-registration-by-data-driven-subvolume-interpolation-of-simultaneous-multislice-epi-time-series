function outputTensor = unMosaic_libmri(mosaic, n_row_blocks,n_col_blocks,n_slice_rows,n_slice_cols)
    if nargin == 1
        %default params, will never change (in princile unless we acquire new data )
        n_row_blocks=6;
        n_col_blocks=6;
        n_slice_rows=64;
        n_slice_cols=64;
    end
    k=1;
    for i=0:n_row_blocks-1
        for j=0:n_col_blocks-1
            curr_row_start = i*n_slice_rows+1;
            curr_col_start = j*n_slice_cols+1;
            outputTensor(:,:,k) = mosaic(curr_row_start:curr_row_start+n_slice_rows-1, curr_col_start:curr_col_start+n_slice_cols-1);
            k=k+1;
        end
    end
end
