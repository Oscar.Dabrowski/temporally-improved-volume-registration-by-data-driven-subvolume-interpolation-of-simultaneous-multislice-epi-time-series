function [files, hdr] =  convertDICOM2NII_v3_libmri(pathToDICOMFiles,outputDir,currDir)
cd(pathToDICOMFiles);
%run this in the folder containing the DCM files to be converted to NII which is needed for SPM

files = spm_select('List',pwd)
%files = spm_select('List',pathToDICOMFiles) %-> does not work for
%spm_dicom_headers(...) (it assumes files are in current folder)
hdr = spm_dicom_headers(files)
%this one converts to NII (output= current folder)
%meta=true % meta is a boolean arg
spm_dicom_convert(hdr,'all','flat','nii',outputDir,true)%(Headers,opts,RootDirectory,format,OutputDirectory,meta)
cd(currDir);%go back to dir which called this script
end