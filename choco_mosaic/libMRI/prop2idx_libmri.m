function idx = prop2idx_libmri(prop, props_opt)
%UNTITLED Summary of this function goes here
%   returns index associated with proportion (hardcoded) this is useful for
%   pairwise rec v2; REM: props = [0.9 0.63 0.37 0.1] (alha index :
%   backwards because alpha is %prev)

%!!! alha index : backwards because alpha is %prev !!!
props = [0.9 0.63 0.37 0.1];%[0.1 0.37 0.63 0.9];
if nargin==2%in case we want to change the hardcoded props in the future (but most likely we will not)
    props=props_opt;
end
idx = find(props ==  prop);
if isempty(idx)
    error('>>>>>>>>>> ERROR: idx is empty (in prop2idx_libmri)');
end
end

