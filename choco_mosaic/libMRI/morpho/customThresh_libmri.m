function [mosaic2D_th,mosaic3D_th] = customThresh_libmri(mosaic2D, n_slice_rows, n_slice_cols, block_size)
%CUSTOMTHRESH Summary of this function goes here
%   the 'magic' values here might need to be changed for other EPI acq
%   parameters e.g. matrix size other than 64x64
if nargin == 1
    perc = quantile(mosaic2D(:),100);
    th = perc(69);%69th percentile
    mosaic2D_th = imerode( imclose( mosaic2D > th, strel('disk',2)), strel('disk',2));
    mosaic3D_th = extract_single_mosaic(mosaic2D_th,block_size,block_size,n_slice_rows,n_slice_cols);
else %if nargin == 2 ---> i.e a th_arg is given (but not used here)
    perc = quantile(mosaic2D(:),100);
    th = perc(89);%69th percentile
    mosaic2D_th = imerode( imclose( mosaic2D > th, strel('disk',2)), strel('disk',2));
    mosaic3D_th = extract_single_mosaic(mosaic2D_th,block_size,block_size,n_slice_rows,n_slice_cols);
end

end

