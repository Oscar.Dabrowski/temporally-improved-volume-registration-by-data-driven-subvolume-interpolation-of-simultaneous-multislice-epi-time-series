clc;clear all;close all;
%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is a small working demo showing how to extract  
% temporally improved (and spatially interpolated) time series from one
% acquired  SMS-EPI time series for a given subject (volunteer): 
% our "subject#2" (s2) from which rigid-body motion parameters can be 
% estimated  by registration  of the first (reference) volume with SPM. 
% (This is also performed by this script and the results are saved 
% in ./data/results.) 
%
% The temporally improved time series can then be used to compute a better 
% estimation of the rigid-body motion parameters as explained in the paper:
% "Choreography Controlled (ChoCo) brain MRI artifact generation for
% labeled motion-corrupted datasets"
% https://doi.org/10.1016/j.ejmp.2022.09.005
% https://www.sciencedirect.com/science/article/pii/S1120179722020476
% which can be cited as (with bibtex):
% @article{DABROWSKI202279,
% title = {Choreography Controlled (ChoCo) brain MRI artifact generation for labeled motion-corrupted datasets},
% journal = {Physica Medica},
% volume = {102},
% pages = {79-87},
% year = {2022},
% issn = {1120-1797},
% doi = {https://doi.org/10.1016/j.ejmp.2022.09.005},
% url = {https://www.sciencedirect.com/science/article/pii/S1120179722020476},
% author = {Oscar Dabrowski and Sébastien Courvoisier and Jean-Luc Falcone and Antoine Klauser and Julien Songeon and Michel Kocher and Bastien Chopard and François Lazeyras},
% keywords = {MRI, Motion-tracking, Motion calibration, Motion artifact, Motion estimation, In-vivo, EPI, Spin-Echo},
% abstract = {MRI is a non-invasive medical imaging modality that is sensitive to patient motion, which constitutes a major limitation in most clinical applications. Solutions may arise from the reduction of acquisition times or from motion-correction techniques, either prospective or retrospective. Benchmarking the latter methods requires labeled motion-corrupted datasets, which are uncommon. Up to our best knowledge, no protocol for generating labeled datasets of MRI images corrupted by controlled motion has yet been proposed. Hence, we present a methodology allowing the acquisition of reproducible motion-corrupted MRI images as well as validation of the system’s performance by motion estimation through rigid-body volume registration of fast 3D echo-planar imaging (EPI) time series. A proof-of-concept is presented, to show how the protocol can be implemented to provide qualitative and quantitative results. An MRI-compatible video system displays a moving target that volunteers equipped with customized plastic glasses must follow to perform predefined head choreographies. Motion estimation using rigid-body EPI time series registration demonstrated that head position can be accurately determined (with an average standard deviation of about 0.39 degrees). A spatio-temporal upsampling and interpolation method to cope with fast motion is also proposed in order to improve motion estimation. The proposed protocol is versatile and straightforward. It is compatible with all MRI systems and may provide insights on the origins of specific motion artifacts. The MRI and artificial intelligence research communities could benefit from this work to build in-vivo labeled datasets of motion-corrupted MRI images suitable for training/testing any retrospective motion correction or machine learning algorithm.}
% }
%
% REM: most of the functions called here are in the sub-folder libMRI
% REM: the outputs of this demo are stored in ./data/results/ if you run
%this script as-is.
%
%Final remarks:
%This code would need refactoring/cleaning (e.g. removing/simplifying some
%parts which are not needed) but it works and provides a proof of concept
%of the method described in our paper above and should contain enough
%comments and examples (in this main script) to be useful

mainDir = pwd;

addpath('./libMRI/');
addpath('./libMRI/spm12/');
addpath(genpath('./libMRI/pairwiseTforms/'));
addpath(genpath('./libMRI/morpho/'));
addpath(genpath('./data/'));

disp('Entered in main_create_Ts_withLibmri ... ');

%interp matrix path
mtsDir = './data/';

doDicom2Nii = true;
doWrite = true;%do write nifti to resDir (except for debug it shoud always be set to true)

idx = load('./data/mosaicRefAcqTimes.mat');
idx = idx.mosaicRefAcqTimes;
[B,sortedMosaicIdx] = sort(idx);

%noMoFileRef: cellarray containing reference noMotion DICOM file ( with 1st EPI
%volume) for a given subject (position in cellarray == subject ID)
%these are used to compute affine transform matrices:
%findPairwiseTforms_libmri(...)
%
%Note: we put it at the index (2) corresponding to subject, for the provided code
%we only give data for subejct#2
noMoFileRef{2} = './data/s2_nomo_vol_ref/s2_nomo_epi_vol1.dcm'

fn_dicom_RL = './data/for_ts_s1_s6_dicom_nii_clean/epi_dir_list_dicom_RL.txt';
fn_dicom_UD = './data/for_ts_s1_s6_dicom_nii_clean/epi_dir_list_dicom_UD.txt';
fn_nii_RL = './data/for_ts_s1_s6_dicom_nii_clean/epi_dir_list_nii_RL.txt';
fn_nii_UD = './data/for_ts_s1_s6_dicom_nii_clean/epi_dir_list_nii_UD.txt';

%anonymous func to concat current path to the relative paths given by
%readtxt_libmri below (otherwise some other func below will crash if given
%relative paths since they might CD in other dirs ...)
concatStr = @(x) [pwd '/' x];

flist_dicom_RL =  cellfun(concatStr,readtxt_libmri(fn_dicom_RL),'UniformOutput',false);
flist_dicom_UD =  cellfun(concatStr,readtxt_libmri(fn_dicom_UD),'UniformOutput',false);
orig_flist_nii_RL =  cellfun(concatStr,readtxt_libmri(fn_nii_RL),'UniformOutput',false);
orig_flist_nii_UD =  cellfun(concatStr,readtxt_libmri(fn_nii_UD),'UniformOutput',false);

%this cellarray stores paths to .txt files containing the results of
%realignement of each time series (ts) with SPM
%these files comtain matrices of rigid-body motion parameters, 3
%translations and 3 rotations
output_rigidBodyParams_ts_files = {};

dataDir = fullfile(mainDir, '/data/results/ts_data/');
resDir = fullfile(mainDir,'/data/results/');

%3 ts with 12 slices/volume = 36 sl/vol -> cf, paper for details
nb_ts = 3;%9;%number of time series 

doInterp = true;
%should in principle only use pairwiseTform_v2
interpType = 'pairwiseTform_v2' %'pairwiseTform' %'pairwiseTform_v2';%'pairwiseTform'; %'linear3d';%'morphoMask';%'morpho';

idx_ = 1;

for i=2:2%subject idx: only data for s2 is provided for this demo
    for moDir = 0:1%RL or UD
            
            if moDir == 0
                moDirStr = '_RL';
            else
                moDirStr ='_UD';
            end

            %if not doDicom2nii then we assume that the orig nii is already
            %there in orig_flist_nii_ ... 
            if moDir == 0
                dicomDir = flist_dicom_RL{i}
                origNiiFilesDir = orig_flist_nii_RL{i}
            else
                dicomDir = flist_dicom_UD{i}
                origNiiFilesDir = orig_flist_nii_UD{i}
            end
            
            if doDicom2Nii
                %save converted dcm to nii in origNiiFilesDir
                %this is used for metadata (copy orig nii metadata to the new nii that i
                %write at the end of this script)
                %directly passing pwd as arg: pwd is reduced to (string) to the
                %current directory 
                convertDICOM2NII_v3_libmri(dicomDir,origNiiFilesDir,pwd)
            end
            %create ALL the times series (ts) with this function
            if strcmpi(interpType,'pairwiseTform') || strcmpi(interpType,'pairwiseTform_v2')
                [tform_forward, tform_backward]  = findPairwiseTforms_libmri(noMoFileRef{i});
                resDirList = createTs_libmri(sortedMosaicIdx,origNiiFilesDir,dicomDir,doWrite,nb_ts,dataDir,moDirStr,i,mtsDir, doInterp, interpType, tform_forward, tform_backward);
            else
                resDirList = createTs_libmri(sortedMosaicIdx,origNiiFilesDir,dicomDir,doWrite,nb_ts,dataDir,moDirStr,i,mtsDir, doInterp, interpType);
            end
            
            %realign in non-orig dir (like this we keep a copy with the
            %untouched NII files
            for resDirVal = resDirList
                resDirVal
                spm_auto_realign_libmri(resDirVal{1},pwd)
                tmp_file = dir([resDirVal{1} '/*.txt']); 
                tmp_file = fullfile(tmp_file.folder, tmp_file.name)
                output_rigidBodyParams_ts_files{idx_} = tmp_file;
                idx_=idx_+1;
            end
        %end
    end
end

%extract to .png for easy visualization (not needed for real usage)
nii2png_libmri(fullfile(mainDir,'/data/results/ts_data/s2/'),'ax');
fprintf('\n >>> The .nii data of the 3 TS (RL and UD) were extracted in .png in: \n %s \n ', fullfile(mainDir,'/data/results/ts_data/s2/nii2png/'));

fprintf('\n >>>>>>>>>>>> Rigid-body parameters estimated with SPM for each time series are stored in the files:\n');
for f=output_rigidBodyParams_ts_files
    fprintf('%s\n',f{1});
end
fprintf('\n');

%here we did only our "subject#2" (s2)
%for the demo in the main program loop (1st loop of this script) the
%filenames are stores in this order for subject#2 (the only subject we
%consider for this demo)
fnames_ = {'s2_ts1_RL','s2_ts2_RL','s2_ts3_RL','s2_ts1_UD','s2_ts2_UD','s2_ts3_UD'};
%save plots of estimated rigid-body params for each time series
ts_rigid_params = cell(6,2);%2 set of params (i.e. translatsion and rotations) for each of the 2*3 TS (RL and UD)
for i=1:2*nb_ts
    [t_xyz, rot_xyz] = getRigidBodyParams_ts_libmri(fnames_{i}, output_rigidBodyParams_ts_files{i}, fullfile(mainDir,'/data/results/'));
    ts_rigid_params(i,:) = {t_xyz, rot_xyz};
end
%save data for further processing, e.g. peak detection etc.
save(fullfile(resDir,'ts_rigid_params.mat'),'ts_rigid_params')

%info for how to index "ts_rigid_params":
%first index: time series (1 to 6 : ts1 RL, ts2 RL, ts3 RL, ts1 UD, ts2 UD,
%ts3 UD)
%second index: 1 : translation, 2: rotation
%third index: pitch, roll or yaw

cd(mainDir)
%demo to show how to build the upsampled interleaved signal (here for RL
%yaw roational motion):
ts1_RL_yaw = ts_rigid_params{1,2}(:,3);
ts2_RL_yaw = ts_rigid_params{2,2}(:,3);
ts3_RL_yaw = ts_rigid_params{3,2}(:,3);

%transpose to get a column vector
upsampled_RL_yaw = upsample3ts_libmri(ts1_RL_yaw,ts2_RL_yaw,ts3_RL_yaw).';

%compare with non-upsampled time series
spm_auto_realign_libmri(fullfile(mainDir,'/data/for_ts_s1_s6_dicom_nii_clean/s2/nii_RL_orig/'), mainDir)

tmp_f = dir(fullfile(mainDir,'/data/for_ts_s1_s6_dicom_nii_clean/s2/nii_RL_orig/*.txt'));
tmp_f = fullfile(tmp_f.folder, tmp_f.name);

nonUpsampledParams = dlmread(tmp_f);
nonUpsampledYaw = rad2deg(nonUpsampledParams(:,6));%column vector

nonUpsampledYaw_zeroFilled = zeros(length(upsampled_RL_yaw),1);
nonUpsampledYaw_zeroFilled(1:3:end) = nonUpsampledYaw;

nonUpsampInterp = interp1(1:length(nonUpsampledYaw_zeroFilled),nonUpsampledYaw_zeroFilled ,1:length(upsampled_RL_yaw));

%plot upsampled overlayed on non-upsampled (just linearly interpolated)
%to show improvement in abrupt motion detection
fig_upsampled=figure('Position', get(0, 'Screensize'));
plot(-upsampled_RL_yaw,'b','linewidth', 2);grid on;hold on;
plot(-nonUpsampInterp,'r','linewidth',2);
title('3x upsampled signal vs non-upsampled');
legend('upsampled', 'non-upsamp.')
xlabel('EPI volume');ylabel('angle (degrees)');

saveas(fig_upsampled, fullfile(resDir,'demo_upsampled_vs_nonUpsamp.png'))